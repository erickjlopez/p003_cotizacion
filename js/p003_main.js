function calcular(){
    let precio = document.getElementById('txtValor').value;
    if (precio == '') {
        alert("El valor del automóvil es requerido");
        return;
    }
    let cmbPlanes = document.getElementById('cmbPlanes').value;
    let interes;

    if (cmbPlanes == 12) {
        interes = (0.125);
    } else if (cmbPlanes == 18){
        interes = (0.172);
    } else if (cmbPlanes == 24){
        interes = (0.21);
    } else if (cmbPlanes == 36){
        interes = (0.26);
    } else if (cmbPlanes == 48){
        interes = (0.45);
    }

    let interesT = interes;
    let enganche = precio*0.3;

    let Enganche = document.getElementById('txtEnganche');
    Enganche.innerHTML = Enganche.setAttribute("value", (enganche));

    let TotalF = document.getElementById('txtFinanciar');
    totalfinanciar = ((precio - enganche) + ((precio - enganche) * interesT));
    TotalF.innerHTML = TotalF.setAttribute("value", totalfinanciar);

    let PagoM = document.getElementById('txtPago');
    PagoM.innerHTML = PagoM.setAttribute("value", totalfinanciar/cmbPlanes);

}

function limpiar(){
    
    let Enganche = document.getElementById('txtEnganche');
    Enganche.innerHTML = Enganche.setAttribute("value", "");
    
    let TotalF = document.getElementById('txtFinanciar');
    TotalF.innerHTML = TotalF.setAttribute("value", "");

    let PagoM = document.getElementById('txtPago');
    PagoM.innerHTML = PagoM.setAttribute("value", "");

    let Precio = document.getElementById('txtValor');
    Precio.value = "";
    
    let meses = document.getElementById('cmbPlanes');
    meses.value = 12;

}